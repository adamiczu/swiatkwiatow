## Instalacja

1. git clone git@gitlab.com:adamiczu/swiatkwiatow.git
2. cd swiatkwiatow
3. composer install
4. php bin/console doctrine:migrations:migrate

## Uruchomienie

1. php bin/console app:download-image

## Testy

1. php ./vendor/bin/phpunit
