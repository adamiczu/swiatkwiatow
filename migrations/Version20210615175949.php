<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210615175949 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE image_catalog (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, original_name VARCHAR(64) NOT NULL, local_name VARCHAR(64) NOT NULL)');
        $this->addSql('CREATE INDEX image_catalog_original_name_idx ON image_catalog (original_name)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE image_catalog');
    }
}
