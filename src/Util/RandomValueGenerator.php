<?php

declare(strict_types=1);

namespace App\Util;

final class RandomValueGenerator
{
    public static function getRandomInteger(int $min, int $max): int
    {
        return random_int($min, $max);
    }

    public static function getRandomString(int $length): string
    {
        return md5(random_bytes($length));
    }
}
