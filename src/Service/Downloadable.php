<?php

declare(strict_types=1);

namespace App\Service;

interface Downloadable
{
    public function download(string $url, ?string $customName = null): void;
}
