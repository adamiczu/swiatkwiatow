<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\ImageCatalog;
use App\Repository\ImageCatalogRepository;
use Doctrine\ORM\EntityManagerInterface;

final class ImageCatalogManager
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getImagesAmount(): int
    {
        return $this->getRepository()->count([]);
    }

    public function imageExists(string $originalName): bool
    {
        return $this->getRepository()->findOneBy(['originalName' => $originalName]) !== null;
    }

    public function save(string $originalName, string $localName): void
    {
        $imageCatalog = new ImageCatalog($originalName, $localName);
        $this->entityManager->persist($imageCatalog);
        $this->entityManager->flush();
    }

    private function getRepository(): ImageCatalogRepository
    {
        return $this->entityManager->getRepository(ImageCatalog::class);
    }
}
