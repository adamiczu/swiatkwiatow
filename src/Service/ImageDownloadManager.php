<?php

declare(strict_types=1);

namespace App\Service;

final class ImageDownloadManager extends DownloadManager
{
    public function download(string $url, ?string $customName = null): void
    {
        $directory = $this->getDirectory('images');
        $path = sprintf('%s/%s', $directory, $customName);
        $this->saveFile($path, $url);
    }
}
