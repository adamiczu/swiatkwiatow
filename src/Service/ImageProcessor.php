<?php

declare(strict_types=1);

namespace App\Service;

use App\Exception\EmptyImagesArrayException;
use App\Exception\ImageExistsException;
use App\Util\RandomValueGenerator;
use Symfony\Component\DomCrawler\Crawler;

final class ImageProcessor
{
    private HtmlDownloadManager $htmlDownloadManager;
    private ImageDownloadManager $imageDownloadManager;
    private ImageCatalogManager $imageCatalogManager;
    private array $images = [];

    public function __construct(
        HtmlDownloadManager $htmlDownloadManager,
        ImageDownloadManager $imageDownloadManager,
        ImageCatalogManager $imageCatalogManager
    )
    {
        $this->htmlDownloadManager = $htmlDownloadManager;
        $this->imageDownloadManager = $imageDownloadManager;
        $this->imageCatalogManager = $imageCatalogManager;
    }

    public function downloadFiles(string $siteUrl, int $amount): void
    {
        $this->getImages($siteUrl);

        for ($i = 0; $i < $amount; $i++) {
            try {
                $url = $this->getRandomImageUrl();
                $originalName = $this->getOriginalName($url);

                if ($this->imageCatalogManager->imageExists($originalName)) {
                    throw new ImageExistsException($originalName);
                }

                $localName = $this->getLocalName($originalName);
                $this->imageDownloadManager->download($url, $localName);
                $this->imageCatalogManager->save($originalName, $localName);
            } catch (ImageExistsException $e) {
                if ($this->imageCatalogManager->getImagesAmount() !== $this->getImagesAmount()) {
                    $i--;
                }
            }
        }
    }

    private function getImages(string $siteUrl): void
    {
        $this->htmlDownloadManager->download($siteUrl);
        $html = $this->htmlDownloadManager->getFile();
        $crawler = new Crawler($html);
        $this->images = $crawler->filterXPath("//img[contains(@src, '{$siteUrl}')]")->images();
    }

    private function getImagesAmount(): int
    {
        return count($this->images);
    }

    private function getOriginalName(string $url): string
    {
        return substr($url, strrpos($url, '/') + 1);
    }

    private function getLocalName(string $originalName): string
    {
        $extension = substr($originalName, strrpos($originalName, '.') + 1);

        return sprintf('%s.%s', RandomValueGenerator::getRandomString(10), $extension);
    }

    private function getRandomImageUrl(): string
    {
        if (empty($this->images)) {
            throw new EmptyImagesArrayException();
        }

        return $this->images[RandomValueGenerator::getRandomInteger(0, $this->getImagesAmount() - 1)]->getUri();
    }
}
