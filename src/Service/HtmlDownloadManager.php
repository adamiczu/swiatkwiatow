<?php

declare(strict_types=1);

namespace App\Service;

final class HtmlDownloadManager extends DownloadManager
{
    public function download(string $url, ?string $customName = null): void
    {
        $directory = $this->getDirectory('html');
        $path = sprintf('%s/%s', $directory, $customName ?? 'index.html');
        $this->saveFile($path, $url);
    }
}
