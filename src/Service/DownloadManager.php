<?php

declare(strict_types=1);

namespace App\Service;

use App\Exception\DirectoryNotFoundException;
use App\Exception\NotYetDownloadedException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

abstract class DownloadManager implements Downloadable
{
    protected string $projectDirectory;
    protected string $filePath;

    public function __construct(ParameterBagInterface $parameterBag)
    {
        $this->projectDirectory = $parameterBag->get('kernel.project_dir');
    }

    protected function getDirectory(string $directory): string
    {
        $path = sprintf('%s/tmp/%s', $this->projectDirectory, $directory);

        if (!is_dir($path)) {
            throw new DirectoryNotFoundException($path);
        }

        return $path;
    }

    protected function saveFile(string $path, string $url): void
    {
        file_put_contents($path, file_get_contents($url));
        $this->filePath = $path;
    }

    public function getFile(): string
    {
        if (!$this->filePath) {
            throw new NotYetDownloadedException();
        }

        return file_get_contents($this->filePath);
    }
}
