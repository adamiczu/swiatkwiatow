<?php

declare(strict_types=1);

namespace App\Command;

use App\Exception\AppException;
use App\Service\ImageProcessor;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DownloadImageCommand extends Command
{
    protected static $defaultName = 'app:download-image';

    private ImageProcessor $imageProcessor;

    public function __construct(ImageProcessor $imageProcessor)
    {
        $this->imageProcessor = $imageProcessor;
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $this->imageProcessor->downloadFiles('https://sklep.swiatkwiatow.pl', 3);
        } catch (AppException $e) {
            $output->writeln("<error>{$e->getMessage()}</error>");
            return Command::FAILURE;
        }
        return Command::SUCCESS;
    }
}
