<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\ImageCatalog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ImageCatalog|null find($id, $lockMode = null, $lockVersion = null)
 * @method ImageCatalog|null findOneBy(array $criteria, array $orderBy = null)
 * @method ImageCatalog[]    findAll()
 * @method ImageCatalog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ImageCatalogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ImageCatalog::class);
    }
}
