<?php

declare(strict_types=1);

namespace App\Exception;

final class ImageExistsException extends AppException
{
    public function __construct(string $originalName)
    {
        parent::__construct(sprintf('Image "%s" already exists!', $originalName));
    }
}
