<?php

declare(strict_types=1);

namespace App\Exception;

final class EmptyImagesArrayException extends AppException
{
    public function __construct()
    {
        parent::__construct('Empty images array!');
    }
}
