<?php

declare(strict_types=1);

namespace App\Exception;

final class NotYetDownloadedException extends AppException
{
    public function __construct()
    {
        parent::__construct('File not yet downloaded!');
    }
}
