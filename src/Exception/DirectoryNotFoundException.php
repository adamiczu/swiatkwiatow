<?php

declare(strict_types=1);

namespace App\Exception;

final class DirectoryNotFoundException extends AppException
{
    public function __construct(string $path)
    {
        parent::__construct(sprintf('Directory "%s" not found!', $path));
    }
}
