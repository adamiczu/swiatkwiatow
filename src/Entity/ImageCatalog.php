<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\ImageCatalogRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ImageCatalogRepository::class)
 * @ORM\Table(name="image_catalog",indexes={@ORM\Index(name="image_catalog_original_name_idx", columns={"original_name"})})
 */
class ImageCatalog
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private string $originalName;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private string $localName;

    public function __construct(string $originalName, string $localName)
    {
        $this->originalName = $originalName;
        $this->localName = $localName;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOriginalName(): string
    {
        return $this->originalName;
    }

    public function setOriginalName(string $originalName): self
    {
        $this->originalName = $originalName;

        return $this;
    }

    public function getLocalName(): string
    {
        return $this->localName;
    }

    public function setLocalName(string $localName): self
    {
        $this->localName = $localName;

        return $this;
    }
}
