<?php

namespace App\Tests\Util;

use App\Util\RandomValueGenerator;
use PHPUnit\Framework\TestCase;

class RandomValueGeneratorTest extends TestCase
{
    public function testGetRandomInteger(): void
    {
        $min = 0;
        $max = 100;
        $randomInteger = RandomValueGenerator::getRandomInteger($min, $max);

        $this->assertIsInt($randomInteger);
        $this->assertTrue(in_array($randomInteger, range($min, $max)));
    }

    public function testGetRandomString(): void
    {
        $randomString = RandomValueGenerator::getRandomString(10);

        $this->assertIsString($randomString);
    }
}
