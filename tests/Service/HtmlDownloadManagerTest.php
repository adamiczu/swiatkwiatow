<?php

namespace App\Tests\Service;

use App\Service\HtmlDownloadManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class HtmlDownloadManagerTest extends KernelTestCase
{
    public function testDownload(): void
    {
        $url = 'https://sklep.swiatkwiatow.pl';
        self::bootKernel();
        $container = self::$container;
        $htmlDownloadManager = $container->get(HtmlDownloadManager::class);
        $htmlDownloadManager->download($url, 'test.html');

        $this->assertEquals(file_get_contents($url), $htmlDownloadManager->getFile());
    }
}
