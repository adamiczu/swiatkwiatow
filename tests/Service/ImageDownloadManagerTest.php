<?php

namespace App\Tests\Service;

use App\Service\ImageDownloadManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ImageDownloadManagerTest extends KernelTestCase
{
    public function testDownload(): void
    {
        $url = 'https://sklep.swiatkwiatow.pl/images/thumbnails/230/230/detailed/57/burak-cwiklowy-opolski_yrkv-0y.jpg';
        self::bootKernel();
        $container = self::$container;
        $htmlDownloadManager = $container->get(ImageDownloadManager::class);
        $htmlDownloadManager->download($url, 'test.jpg');

        $this->assertEquals(file_get_contents($url), $htmlDownloadManager->getFile());
    }
}
